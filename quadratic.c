#include <stdio.h>
#include <math.h>
int main()
{
     int a,b,c,d;
     float r1,r2;
     printf("Enter the coefficiets of the quadratic equation\n");
     scanf("%d%d%d",&a,&b,&c);
     d=b*b-4*a*c;
     r1=(-b+sqrt(d))/(2*a);
     r2=(-b-sqrt(d))/(2*a);
     if(d==0)
     {
         printf("the roots are equal\n");
         printf("root1=%f,root2=%f",r1,r2);
     }
     else if(d>0)
     {
         printf("the roots are distinct\n");
         printf("root1=%f,root2=%f",r1,r2);
     }
     else
     {
         printf("the roots are imaginary\n");
         printf("root1=%f+i%f,root2=%f-i%f\n",(float)(-b)/(2*a),(float)(sqrt(-d))/(2*a),(float)(-b)/(2*a),(float)(sqrt(-d))/(2*a));
     }
     return 0;
}