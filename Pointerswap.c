#include <stdio.h>
void swap(int *p,int *q);
int main()
{
	int x,y;
	printf("enter two numbers\n");
	scanf("%d%d",&x,&y);
	printf("before swaping x=%d and y=%d\n",x,y);
	swap(&x,&y);
	printf("after swaping x=%d and y=%d\n",x,y);
	return 0;
}
void swap(int *p,int *q)
{
	int t;
	t=*p;
	*p=*q;
	*q=t;
}